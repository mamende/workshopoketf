data "oci_containerengine_cluster_option" "WorkshopOKEClusterOption" {
  cluster_option_id = "all"
}

data "oci_containerengine_node_pool_option" "WorkshopOKEClusterNodePoolOption" {
  node_pool_option_id = "all"
}

output "Workshop_Cluster_Kubernetes_Versions" {
  value = [data.oci_containerengine_cluster_option.WorkshopOKEClusterOption.kubernetes_versions]
}

output "Workshop_Cluster_NodePool_Kubernetes_Version" {
  value = [data.oci_containerengine_node_pool_option.WorkshopOKEClusterNodePoolOption.kubernetes_versions]
}
