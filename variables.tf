variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "compartment_ocid" {}
variable "region" {}
variable "private_key_oci" {}
variable "public_key_oci" {}

variable "VCN-CIDR" {
  default = "10.0.0.0/16"
}

variable "cluster_options_kubernetes_network_config_pods_cidr" {
  default = "10.1.0.0/16"
}

variable "cluster_options_kubernetes_network_config_services_cidr" {
  default = "10.2.0.0/16"
}

variable "node_pool_quantity_per_subnet" {
  default = 1
}

variable "kubernetes_version" {
  default = "v1.17.9"
}

variable "ADs" {
  default = ["SNBG:EU-FRANKFURT-1-AD-1", "SNBG:EU-FRANKFURT-1-AD-2", "SNBG:EU-FRANKFURT-1-AD-3"]
}

variable "Shapes" {
 default = ["VM.Standard.E2.1","VM.Standard.E2.1.Micro","VM.Standard2.1","VM.Standard.E2.1","VM.Standard.E2.2"]
}

variable "Images" {
 # Oracle-Linux-7.8-2020.08.26-0 in Frankfurt
 default = ["ocid1.image.oc1.eu-frankfurt-1.aaaaaaaa2hf6w7wzljql3vozjqu77di52wvedqcfrhcqbmypn4bqvmhjgabq"]
}

variable "ClusterName" {
  default = "WorkshopOKECluster"
}

