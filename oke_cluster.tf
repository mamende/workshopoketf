
resource "oci_containerengine_cluster" "WorkshopOKECluster" {
  #depends_on = [oci_identity_policy.WorkshopOKEPolicy1]
  compartment_id     = oci_identity_compartment.WorkshopOKECompartment.id
  kubernetes_version = var.kubernetes_version
  name               = var.ClusterName
  vcn_id             = oci_core_vcn.WorkshopVCN.id

  options {
    service_lb_subnet_ids = [oci_core_subnet.WorkshopClusterSubnet1.id, oci_core_subnet.WorkshopClusterSubnet2.id]

    add_ons {
      is_kubernetes_dashboard_enabled = true
      is_tiller_enabled               = true
    }

    kubernetes_network_config {
      pods_cidr     = var.cluster_options_kubernetes_network_config_pods_cidr
      services_cidr = var.cluster_options_kubernetes_network_config_services_cidr
    }
  }
}

resource "oci_containerengine_node_pool" "WorkshopOKENodePool" {
  #depends_on = [oci_identity_policy.WorkshopOKEPolicy1]
  cluster_id         = oci_containerengine_cluster.WorkshopOKECluster.id
  compartment_id     = oci_identity_compartment.WorkshopOKECompartment.id
  kubernetes_version = var.kubernetes_version
  name               = "WorkshopOKENodePool"
  node_image_name    = var.Images[0]
  node_shape         = var.Shapes[0]
  subnet_ids         = [oci_core_subnet.WorkshopNodePoolSubnet1.id,oci_core_subnet.WorkshopNodePoolSubnet2.id]

  initial_node_labels {
    key   = "key"
    value = "value"
  }

  quantity_per_subnet = var.node_pool_quantity_per_subnet
  ssh_public_key      = file(var.public_key_oci)
}

output "WorkshopOKECluster" {
  value = {
    id                 = oci_containerengine_cluster.WorkshopOKECluster.id
    kubernetes_version = oci_containerengine_cluster.WorkshopOKECluster.kubernetes_version
    name               = oci_containerengine_cluster.WorkshopOKECluster.name
  }
}

output "WorkshopOKENodePool" {
  value = {
    id                 = oci_containerengine_node_pool.WorkshopOKENodePool.id
    kubernetes_version = oci_containerengine_node_pool.WorkshopOKENodePool.kubernetes_version
    name               = oci_containerengine_node_pool.WorkshopOKENodePool.name
    subnet_ids         = oci_containerengine_node_pool.WorkshopOKENodePool.subnet_ids
  }
}
