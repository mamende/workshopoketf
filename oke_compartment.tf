resource "oci_identity_compartment" "WorkshopOKECompartment" {
  name = "WorkshopOKECompartment"
  description = "Workshop Compartment"
  compartment_id = var.compartment_ocid
}
