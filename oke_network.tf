resource "oci_core_vcn" "WorkshopVCN" {
  cidr_block     = var.VCN-CIDR
  compartment_id = oci_identity_compartment.WorkshopOKECompartment.id
  display_name   = "WorkshopVCN"
}

resource "oci_core_internet_gateway" "WorkshopInternetGateway" {
  compartment_id = oci_identity_compartment.WorkshopOKECompartment.id
  display_name   = "WorkshopInternetGateway"
  vcn_id         = oci_core_vcn.WorkshopVCN.id
}

resource "oci_core_route_table" "WorkshopRouteTableViaIGW" {
  compartment_id = oci_identity_compartment.WorkshopOKECompartment.id
  vcn_id         = oci_core_vcn.WorkshopVCN.id
  display_name   = "WorkshopRouteTableViaIGW"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id =  oci_core_internet_gateway.WorkshopInternetGateway.id
  }
}

resource "oci_core_security_list" "WorkshopOKESecurityList" {
    compartment_id = oci_identity_compartment.WorkshopOKECompartment.id
    display_name = "WorkshopOKESecurityList"
    vcn_id = oci_core_vcn.WorkshopVCN.id
    
    egress_security_rules {
        protocol = "All"
        destination = "0.0.0.0/0"
    }

    /* This entry is necesary for DNS resolving (open UDP traffic). */
    ingress_security_rules {
        protocol = "17"
        source = var.VCN-CIDR
    }
}

resource "oci_core_subnet" "WorkshopClusterSubnet1" {
  availability_domain = var.ADs[0]
  cidr_block          = "10.0.1.0/24"
  compartment_id      = oci_identity_compartment.WorkshopOKECompartment.id
  vcn_id              = oci_core_vcn.WorkshopVCN.id
  display_name        = "WorkshopClusterSubnet1"

  security_list_ids = [oci_core_vcn.WorkshopVCN.default_security_list_id, oci_core_security_list.WorkshopOKESecurityList.id]
  route_table_id    = oci_core_route_table.WorkshopRouteTableViaIGW.id
}

resource "oci_core_subnet" "WorkshopClusterSubnet2" {
  availability_domain = var.ADs[1]
  cidr_block          = "10.0.2.0/24"
  compartment_id      = oci_identity_compartment.WorkshopOKECompartment.id
  vcn_id              = oci_core_vcn.WorkshopVCN.id
  display_name        = "WorkshopClusterSubnet2"

  security_list_ids = [oci_core_vcn.WorkshopVCN.default_security_list_id, oci_core_security_list.WorkshopOKESecurityList.id]
  route_table_id    = oci_core_route_table.WorkshopRouteTableViaIGW.id
}

resource "oci_core_subnet" "WorkshopNodePoolSubnet1" {
  availability_domain = var.ADs[0]
  cidr_block          = "10.0.3.0/24"
  compartment_id      = oci_identity_compartment.WorkshopOKECompartment.id
  vcn_id              = oci_core_vcn.WorkshopVCN.id
  display_name        = "WorkshopNodePoolSubnet1"

  security_list_ids = [oci_core_vcn.WorkshopVCN.default_security_list_id, oci_core_security_list.WorkshopOKESecurityList.id]
  route_table_id    = oci_core_route_table.WorkshopRouteTableViaIGW.id
}

resource "oci_core_subnet" "WorkshopNodePoolSubnet2" {
  availability_domain = var.ADs[1]
  cidr_block          = "10.0.4.0/24"
  compartment_id      = oci_identity_compartment.WorkshopOKECompartment.id
  vcn_id              = oci_core_vcn.WorkshopVCN.id
  display_name        = "WorkshopNodePoolSubnet2"

  security_list_ids = [oci_core_vcn.WorkshopVCN.default_security_list_id, oci_core_security_list.WorkshopOKESecurityList.id]
  route_table_id    = oci_core_route_table.WorkshopRouteTableViaIGW.id
}
